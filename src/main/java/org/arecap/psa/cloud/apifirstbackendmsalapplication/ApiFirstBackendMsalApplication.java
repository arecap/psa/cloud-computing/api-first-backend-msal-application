package org.arecap.psa.cloud.apifirstbackendmsalapplication;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan
@ComponentScan( "org.arecap.psa.cloud.openapi.springboot.web3.petstore")
@ComponentScan( "org.arecap.psa.cloud.swagger.v3.springboot.petstore")
//@ComponentScan(basePackageClasses = {PetsApiController.class})
public class ApiFirstBackendMsalApplication extends SpringBootServletInitializer {

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(ApiFirstBackendMsalApplication.class);
	}

	public static void main(String[] args) {
		new ApiFirstBackendMsalApplication().configure(new SpringApplicationBuilder(ApiFirstBackendMsalApplication.class)).run(args);
	}
}
