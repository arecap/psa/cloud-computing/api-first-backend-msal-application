package org.arecap.psa.cloud.apifirstbackendmsalapplication.web.petstore;

import io.github.resilience4j.bulkhead.Bulkhead;
import io.github.resilience4j.bulkhead.BulkheadRegistry;
import io.github.resilience4j.bulkhead.ThreadPoolBulkhead;
import io.github.resilience4j.bulkhead.ThreadPoolBulkheadRegistry;
import io.github.resilience4j.circuitbreaker.CircuitBreaker;
import io.github.resilience4j.circuitbreaker.CircuitBreakerRegistry;
import io.github.resilience4j.ratelimiter.RateLimiter;
import io.github.resilience4j.ratelimiter.RateLimiterRegistry;
import io.github.resilience4j.retry.Retry;
import io.github.resilience4j.retry.RetryRegistry;
import io.github.resilience4j.timelimiter.TimeLimiter;
import io.github.resilience4j.timelimiter.TimeLimiterRegistry;
import org.arecap.psa.cloud.resilience4j.SimpleSupplierResilienceService;
import org.arecap.psa.cloud.swagger.v3.springboot.petstore.api.PetsV2ApiDelegate;
import org.arecap.psa.cloud.swagger.v3.springboot.petstore.model.NewPet;
import org.arecap.psa.cloud.swagger.v3.springboot.petstore.model.Pet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

@Service
public class PetstoreApiV2 implements PetsV2ApiDelegate, SimpleSupplierResilienceService {

    private static final String BACKEND_B = "backendB";

    private final CircuitBreaker circuitBreaker;
    private final Bulkhead bulkhead;
    private final ThreadPoolBulkhead threadPoolBulkhead;
    private final Retry retry;
    private final RateLimiter rateLimiter;
    private final TimeLimiter timeLimiter;
    private final ScheduledExecutorService scheduledExecutorService;

    @Autowired
    private ErrorService errorService;

    public PetstoreApiV2(
            CircuitBreakerRegistry circuitBreakerRegistry,
            ThreadPoolBulkheadRegistry threadPoolBulkheadRegistry,
            BulkheadRegistry bulkheadRegistry,
            RetryRegistry retryRegistry,
            RateLimiterRegistry rateLimiterRegistry,
            TimeLimiterRegistry timeLimiterRegistry
    ) {
        this.circuitBreaker = circuitBreakerRegistry.circuitBreaker(BACKEND_B);
        this.bulkhead = bulkheadRegistry.bulkhead(BACKEND_B);
        this.threadPoolBulkhead = threadPoolBulkheadRegistry.bulkhead(BACKEND_B);
        this.retry = retryRegistry.retry(BACKEND_B);
        this.rateLimiter = rateLimiterRegistry.rateLimiter(BACKEND_B);
        this.timeLimiter = timeLimiterRegistry.timeLimiter(BACKEND_B);
        this.scheduledExecutorService = Executors.newScheduledThreadPool(3);
    }

    @Override
    public CircuitBreaker getCircuitBreaker() {
        return circuitBreaker;
    }

    @Override
    public Bulkhead getBulkhead() {
        return bulkhead;
    }

    @Override
    public ThreadPoolBulkhead getThreadPoolBulkhead() {
        return threadPoolBulkhead;
    }

    @Override
    public Retry getRetry() {
        return retry;
    }

    @Override
    public RateLimiter getRateLimiter() {
        return rateLimiter;
    }

    @Override
    public TimeLimiter getTimeLimiter() {
        return timeLimiter;
    }

    @Override
    public ScheduledExecutorService getScheduledExecutorService() {
        return scheduledExecutorService;
    }

    private ResponseEntity<List<Pet>> fallBackListPets(Throwable throwable) {
        return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
    }

    private ResponseEntity<List<Pet>> listPetsService(int limit) {
        System.out.println("Error Service error count:\t" + errorService.getErrorCount());
        if(errorService.getErrorCount() == null || errorService.getErrorCount() == 0) {
            errorService.setErrorCount(1);
            throw new RuntimeException("error count not set");
        }
        if(errorService.getErrorCount() < 3) {
            errorService.setErrorCount(errorService.getErrorCount() + 1);
            throw new RuntimeException("error count set to:\t" + errorService.getErrorCount());
        }
        return new ResponseEntity(HttpStatus.NOT_IMPLEMENTED);
    }


    @Override
    public ResponseEntity<Pet> addPet(NewPet body) {
        return execute(() -> addPetService(body));
    }

    private ResponseEntity<Pet> addPetService(NewPet body) {
        return new ResponseEntity(HttpStatus.NOT_IMPLEMENTED);
    }

    @Override
    public ResponseEntity<Pet> findPetById(Long id) {
        return execute(() -> findPetByIdService(id));
    }

    private ResponseEntity<Pet> findPetByIdService(Long id) {
        return new ResponseEntity(HttpStatus.NOT_IMPLEMENTED);
    }

    @Override
    public ResponseEntity<List<Pet>> findPets(List<String> tags, Integer limit) {
        return executeWithFallback(() -> listPetsService(limit), this::fallBackListPets);
    }
}
