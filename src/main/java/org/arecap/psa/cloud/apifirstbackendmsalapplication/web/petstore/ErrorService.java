package org.arecap.psa.cloud.apifirstbackendmsalapplication.web.petstore;

import org.springframework.stereotype.Service;

@Service
public class ErrorService {

    private Integer errorCount;

    public Integer getErrorCount() {
        return errorCount;
    }

    public void setErrorCount(Integer errorCount) {
        this.errorCount = errorCount;
    }
}
