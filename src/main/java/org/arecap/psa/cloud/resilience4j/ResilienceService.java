package org.arecap.psa.cloud.resilience4j;

import io.github.resilience4j.bulkhead.Bulkhead;
import io.github.resilience4j.bulkhead.ThreadPoolBulkhead;
import io.github.resilience4j.circuitbreaker.CircuitBreaker;
import io.github.resilience4j.ratelimiter.RateLimiter;
import io.github.resilience4j.retry.Retry;
import io.github.resilience4j.timelimiter.TimeLimiter;

import java.util.concurrent.ScheduledExecutorService;

public interface ResilienceService {

    CircuitBreaker getCircuitBreaker();

    Bulkhead getBulkhead();

    ThreadPoolBulkhead getThreadPoolBulkhead();

    Retry getRetry();

    RateLimiter getRateLimiter();

    TimeLimiter getTimeLimiter();

    ScheduledExecutorService getScheduledExecutorService();


}
