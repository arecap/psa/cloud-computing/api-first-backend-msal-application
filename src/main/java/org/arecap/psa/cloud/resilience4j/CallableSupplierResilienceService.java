package org.arecap.psa.cloud.resilience4j;

import io.github.resilience4j.bulkhead.BulkheadFullException;
import io.github.resilience4j.circuitbreaker.CallNotPermittedException;
import io.github.resilience4j.decorators.Decorators;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeoutException;
import java.util.function.Function;
import java.util.function.Supplier;

import static java.util.Arrays.asList;

public interface CallableSupplierResilienceService extends ResilienceService {

    default <T> CompletableFuture<T> executeAsync(Supplier<T> supplier){
        return Decorators.ofSupplier(supplier)
                .withThreadPoolBulkhead(getThreadPoolBulkhead())
                .withTimeLimiter(getTimeLimiter(), getScheduledExecutorService())
                .withCircuitBreaker(getCircuitBreaker())
                .withRetry(getRetry(), getScheduledExecutorService())
                .get().toCompletableFuture();
    }

    default <T> CompletableFuture<T> executeAsyncWithFallback(Supplier<T> supplier, Function<Throwable, T> fallback){
        return Decorators.ofSupplier(supplier)
                .withThreadPoolBulkhead(getThreadPoolBulkhead())
                .withTimeLimiter(getTimeLimiter(), getScheduledExecutorService())
                .withCircuitBreaker(getCircuitBreaker())
                .withRetry(getRetry(), getScheduledExecutorService())
                .withFallback(asList(TimeoutException.class, CallNotPermittedException.class, BulkheadFullException.class),
                        fallback)
                .get().toCompletableFuture();
    }

}
