package org.arecap.psa.cloud.resilience4j;

import io.github.resilience4j.bulkhead.BulkheadFullException;
import io.github.resilience4j.circuitbreaker.CallNotPermittedException;
import io.github.resilience4j.decorators.Decorators;

import java.util.concurrent.TimeoutException;
import java.util.function.Function;
import java.util.function.Supplier;

import static java.util.Arrays.asList;

public interface SimpleSupplierResilienceService extends ResilienceService {

    default <T> T execute(Supplier<T> supplier){
        return Decorators.ofSupplier(supplier)
                .withCircuitBreaker(getCircuitBreaker())
                .withBulkhead(getBulkhead())
                .withRetry(getRetry())
                .get();
    }

    default <T> T executeWithFallback(Supplier<T> supplier, Function<Throwable, T> fallback) {
        return Decorators.ofSupplier(supplier)
                .withCircuitBreaker(getCircuitBreaker())
                .withBulkhead(getBulkhead())
                .withRetry(getRetry())
                .withFallback(asList(TimeoutException.class, CallNotPermittedException.class, BulkheadFullException.class),
                        fallback)
                .get();
    }

}
