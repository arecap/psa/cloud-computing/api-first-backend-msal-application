package org.arecap.psa.cloud.resilience4j;

import io.github.resilience4j.bulkhead.BulkheadFullException;
import io.github.resilience4j.circuitbreaker.CallNotPermittedException;
import io.github.resilience4j.reactor.bulkhead.operator.BulkheadOperator;
import io.github.resilience4j.reactor.circuitbreaker.operator.CircuitBreakerOperator;
import io.github.resilience4j.reactor.retry.RetryOperator;
import io.github.resilience4j.reactor.timelimiter.TimeLimiterOperator;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.concurrent.TimeoutException;
import java.util.function.Function;

public interface ReactiveResilienceService extends ResilienceService {

    default <T> Mono<T> execute(Mono<T> publisher){
        return publisher
                .transform(BulkheadOperator.of(getBulkhead()))
                .transform(CircuitBreakerOperator.of(getCircuitBreaker()))
                .transform(RetryOperator.of(getRetry()));
    }

    default <T> Flux<T> execute(Flux<T> publisher){
        return publisher
                .transform(BulkheadOperator.of(getBulkhead()))
                .transform(CircuitBreakerOperator.of(getCircuitBreaker()))
                .transform(RetryOperator.of(getRetry()));
    }


    default <T> Mono<T> executeWithFallback(Mono<T> publisher, Function<Throwable, Mono<T>> fallback){
        return publisher
                .transform(TimeLimiterOperator.of(getTimeLimiter()))
                .transform(BulkheadOperator.of(getBulkhead()))
                .transform(CircuitBreakerOperator.of(getCircuitBreaker()))
                .onErrorResume(TimeoutException.class, fallback)
                .onErrorResume(CallNotPermittedException.class, fallback)
                .onErrorResume(BulkheadFullException.class, fallback);
    }

    default <T> Flux<T> executeWithFallback(Flux<T> publisher, Function<Throwable, Flux<T>> fallback){
        return publisher
                .transform(TimeLimiterOperator.of(getTimeLimiter()))
                .transform(BulkheadOperator.of(getBulkhead()))
                .transform(CircuitBreakerOperator.of(getCircuitBreaker()))
                .onErrorResume(TimeoutException.class, fallback)
                .onErrorResume(CallNotPermittedException.class, fallback)
                .onErrorResume(BulkheadFullException.class, fallback);
    }

}
