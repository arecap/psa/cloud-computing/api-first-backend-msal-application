# Api First Backend MSAL Application

design support API First codegen repository implementation of backend services with design support for Resilience4J Circuit Breaker.
example for [SimpleSupplier](https://gitlab.com/arecap/psa/cloud-computing/api-first-backend-msal-application/-/blob/main/src/main/java/org/arecap/psa/cloud/apifirstbackendmsalapplication/web/petstore/PetstoreApiV2.java) and [CallableSupplier](https://gitlab.com/arecap/psa/cloud-computing/api-first-backend-msal-application/-/blob/main/src/main/java/org/arecap/psa/cloud/apifirstbackendmsalapplication/web/petstore/PetstoreApiV1.java) ResilienceService using backendB configuration

## Implementation details
For resilience4j read this article https://resilience4j.readme.io/docs/getting-started-3
with demo https://github.com/resilience4j/resilience4j-spring-boot2-demo

For API Frist approach with Code Generator see details on [projects](https://gitlab.com/arecap/psa/cloud-computing/spring-boot-api-specification-codegen):
* [Open Api Generator Petstore](https://gitlab.com/arecap/psa/cloud-computing/spring-boot-api-specification-codegen/openapi-generator-petstore)
* [Swagger v3 Codegen Petstore Swagger2](https://gitlab.com/arecap/psa/cloud-computing/spring-boot-api-specification-codegen/swagger-v3-codegen-petstore-swagger2)